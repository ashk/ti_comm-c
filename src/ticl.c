#include <stdlib.h>
#include <stdio.h>
#include "ticl.h"

struct Ticl ticl_create(int tip, int ring) {
    struct Ticl t = { tip, ring };
    return t;
}

void ticl_print(struct Ticl t) {
    printf("client { tip = %d, ring = %d }", t.tip, t.ring);
}
