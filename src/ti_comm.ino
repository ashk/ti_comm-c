#include "Particle.h"
#include "ticl.h"

#define PIN_TIP  D1 // red wire
#define PIN_RING D0 // white wire

int cmd_version(String extra) {
    Serial.println("version...");
    return 0;
}

int cmd_screen_capture(String extra) {
    Serial.println("screen capture...");
    return 1;
}

void setup() {
    Serial.begin(9600);
    Particle.function("screen_capture", cmd_screen_capture);
    Particle.function("version", cmd_version);
}

void loop() {
    // nothing to do here
}
