ti_comm
=======

**buid**

build the firmware from source

```sh
> particle compile photon
```

***flash**

flash the firmware onto your photon device

```sh
> particle flash mydevice myfirmware.bin
```

**open terminal emulator**

where `1234` is the unique number of your device -

```sh
> picocom -b 9600 /dev/cu.usbmodem1234
```

**supported commands**

get version info

```sh
> particle call mydevice version
```

get screen capture

```sh
> particle call mydevice screen_capture
```
